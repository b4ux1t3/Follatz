module Follatz.Collatz
    open Follatz.Primitives

    let collatzMorph num =
        match nModTwo64 num with
        | n0 when n0 = 0 -> divTwo num
        | _ -> threeNPlusOne num
            
    let collatz n =
        match n with
        | n1 when n1 = one64 -> Some n
        | nn when nn < 1 -> None
        | _ -> collatzMorph n |> Some
        
    let rec countCollatz c n =
        match collatz n with
        | Some result -> countCollatz (c+1) result
        | None -> None
        
    let startCollatz n = n |> int64 |> countCollatz 0