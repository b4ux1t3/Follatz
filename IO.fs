module Follatz.IO
    open Follatz.Collatz
    open System

    let printN n = printfn $"%d{n}"

    let printAndReturnCode code errorMessage =
        printfn $"%s{errorMessage}"
        code

    let happyPath n =
        printN n
        0

    let baseErrorMessage =
        $"You need to give me a positive integer less than %d{Int32.MaxValue} as the first argument."

    let overflowErrorMessage =
        $"You gave me a number that probably overflowed when I tried to multiply past %d{Int64.MaxValue}, which is really impressive!"

    let checkCollatz n =
        let result = startCollatz n

        match result with
        | Some res -> happyPath res
        | None -> printAndReturnCode 0xdeadbeef overflowErrorMessage

    let processStringInput (s: string) =
        let parsedArg = s |> Int32.TryParse

        match parsedArg with
        | true, n -> checkCollatz n
        | false, _ -> printAndReturnCode Int32.MinValue $"%s{baseErrorMessage}\nYou gave me %s{s}"
