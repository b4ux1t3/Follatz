﻿open System
open Follatz.IO
             
[<EntryPoint>]
let main args =
    match args with
    | [|s|] -> processStringInput s
    | _     -> printAndReturnCode Int32.MaxValue baseErrorMessage
        