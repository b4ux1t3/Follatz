module Follatz.Primitives
    let two64 = 2 |> int64
    let three64 = 3 |> int64
    let one64 = 1 |> int64
    let threeNPlusOne n = three64 * n + one64
    let divTwo n = n / two64
    let nModTwo64 n = n % two64