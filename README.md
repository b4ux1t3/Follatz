# Follatz

This is meant to be _the_ canonical, idiomatic implementation of the [Collatz Conjecture](https://en.wikipedia.org/wiki/Collatz_conjecture) in F#!

## Goals

The goal here is to take an easily-understood domain and implement it in a way that jives with real-world applications of F#. I want people to see functional programming in action and think "Oh, it's not actually that scary".

If you've never used F# before, this should be a good place to start!

The topics I aim to hit are:

| Feature               | Done? |
|:----------------------|:--|
| Command line IO       | ✅ |
| "Functional first"    | ✅ |
| Pipes and Currying    | ✅ |
| Pattern matching      | ✅ |
| Option type           | ✅ |
| First-class functions | ❌ |
| Algebraic types       | ❌ |

### Not Goals    
The goal here is explicitly NOT to make the fastest, most efficient implementation of the actual algorithm. For example, I'm seriously considering rewriting the `collatzMorph` function to accept a `collatzFunction`, to demonstrate the concepts of first-class functions and mutual recursion.[^1]

Basically, I want to demonstrate as many features of F# as is possible in as trivial a program as possible, such that it can be extrapolated upon by learners into real-world programs.

### Side Goals and Future Plans
I plan on publishing a follow-up project that will touch on more complex topics (at least, from a FP perspective), like disk and network IO. _Maybe_ I'll consider rolling this into a sort of course or something. I dunno.

## Running The Program

You have to have dotnet installed. It _shouldn't_ matter which version. I'm using .NET 8 right now, as it's just what I had installed, but, in theory, it'll work in any version of modern .NET. 

If you have dotnet installed, you just need to run:

```shell
dotnet run -- <NUMBER>
```

That's all. You'll get the number of steps it takes to hit 1, per the Collatz algorithm.

### Future Work

I'll implement a CI/CD "eventually" that'll spit out actual binaries, but that will be more in the pursuit of education around the topic than to produce real packages.
___

[^1]: The main reason I'm not is that I'm not sure mutual recursion is technically idiomatic in F#; since F# is eagerly evaluated and in procedural order, mutual recursion isn't "possible" without passing on of the functions, not like in Haskell.
